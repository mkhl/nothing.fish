# nothing.fish

a [theme](https://fishshell.com/docs/current/cmds/fish_config.html) with almost no colors for the [fish shell](https://fishshell.com/)

## screenshot

![screenshot of `fish_config theme demo`](https://codeberg.org/attachments/94be9c39-e7a2-4631-8ee7-b3df1bf87dd6)

terminal: [foot](https://codeberg.org/dnkl/foot/)  
font: [cascadia code](https://github.com/microsoft/cascadia-code) with cursive

## installation

1. use a terminal emulator that supports italics and dim colors

2. copy/link/move the `.theme` file to your `~/.config/fish/themes/` directory

3. use [`fish_config`](https://fishshell.com/docs/current/cmds/fish_config.html) to apply the theme

    according to the documentation this would be something like

    ```
    fish_config theme choose nothing
    fish_config theme save
    ```

    but that doesn't seem to work 🙃

    instead of saving i ended up looping over the variables set by `fish_config theme choose nothing`
    and promoting each of them to the universal variable
